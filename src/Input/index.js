import React from 'react'

import '../App.css'

export const Input = () => (
	<div className="InputWrap">
		<input type="text" className="Input"/>
		<button className="Button">+</button>
	</div>
)
